package in.travels.traveller.view;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import in.travels.traveller.R;

public class SimpleCalendarDialogFragment extends AppCompatDialogFragment implements OnDateSelectedListener {

    private DateFormat FORMATTER = SimpleDateFormat.getDateInstance();

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();

        //inflate custom layout and get views
        //pass null as parent view because will be in dialog layout
        View view = inflater.inflate(R.layout.date_dialog, null);

        MaterialCalendarView widget = (MaterialCalendarView) view.findViewById(R.id.calendarView);

        TextView txtTitle = (TextView) view.findViewById(R.id.txtViewTitle);
        txtTitle.setText("Trip date");

        widget.setOnDateChangedListener(this);
        widget.setArrowColor(getResources().getColor(R.color.colorPrimary));

        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .setPositiveButton(android.R.string.ok, null)
                .setNegativeButton(android.R.string.cancel, null)
                .create();
    }

    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
        String selectedDate = FORMATTER.format(date.getDate());
    }
}

