package in.travels.traveller.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.List;

import in.travels.traveller.R;

/**
 * Created by udhaya on 26/9/18.
 */

public class DriverInfoFragment extends Fragment {
    private EditText edtTxtDriverName, edtTxtLicenseNo, edtTxtDateOfIssued, edtTxtDateOfBirth,
            edtTxtBloodGroup, edtTxtDateOfJoining;
    private Button btnAddDriver;
    private MaterialSpinner spinnerSalaryType;
    private String selectedSalaryType = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_driver_info,
                container, false);
        getViewCasting(view);
        return view;
    }

    private void getViewCasting(View view) {

        edtTxtDriverName = (EditText) view.findViewById(R.id.edtTxtDriverName);
        edtTxtLicenseNo = (EditText) view.findViewById(R.id.edtTxtLicenseNo);
        edtTxtDateOfIssued = (EditText) view.findViewById(R.id.edtTxtDateOfIssued);
        edtTxtDateOfBirth = (EditText) view.findViewById(R.id.edtTxtDateOfBirth);
        edtTxtBloodGroup = (EditText) view.findViewById(R.id.edtTxtBloodGroup);
        edtTxtDateOfJoining = (EditText) view.findViewById(R.id.edtTxtDateOfJoining);

        spinnerSalaryType = (MaterialSpinner) view.findViewById(R.id.spinnerSalaryType);

        btnAddDriver = (Button) view.findViewById(R.id.btnAddDriver);

        setSalaryType();
    }

    private void setSalaryType() {

        List<String> salaryTypeList = new ArrayList<>();
        salaryTypeList.add(0, "Salary dispensed on every");
        salaryTypeList.add("1 Day");
        salaryTypeList.add("10 Days");
        salaryTypeList.add("20 Days");
        salaryTypeList.add("30 Days");

        spinnerSalaryType.setItems(salaryTypeList);

        spinnerSalaryType.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                String selectedType = item.toString();
                if (!selectedType.equalsIgnoreCase("Salary dispensed on every")) {
                    System.out.println("===========itemSelected" + selectedType);
                    selectedSalaryType = item.toString();

                } else {
                    selectedSalaryType = "";
                }
            }
        });
    }
}
