package in.travels.traveller.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.List;

import in.travels.traveller.R;

/**
 * Created by udhaya on 4/10/18.
 */

public class TripDetailsFragment extends Fragment {

    private MaterialSpinner spinnerFromLoc, spinnerToLoc;
    String selectedFromLocation = "", selectedToLocation = "";
    EditText edtTxtDate;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_trip_details,
                container, false);
        getViewCasting(view);
        return view;
    }

    private void getViewCasting(View view) {

        spinnerFromLoc = (MaterialSpinner) view.findViewById(R.id.spinnerFromLocation);
        spinnerToLoc = (MaterialSpinner) view.findViewById(R.id.spinnerToLocation);

        edtTxtDate = (EditText) view.findViewById(R.id.edtTxtDate);
        edtTxtDate.setInputType(InputType.TYPE_NULL);
        dateListener();
        hideSoftKeyboard(edtTxtDate);

        populateFromLocationInSpinner();
        populateToLocationInSpinner();
    }

    private void dateListener() {
        edtTxtDate.setOnClickListener(v -> {
            new SimpleCalendarDialogFragment().show(getActivity().getSupportFragmentManager(), "Calendar");
        });
    }


    private void populateFromLocationInSpinner() {

        List<String> salaryTypeList = new ArrayList<>();
        salaryTypeList.add(0, "Source location");
        salaryTypeList.add("Bangalore");
        salaryTypeList.add("Chennai");
        salaryTypeList.add("Coimbatore");
        salaryTypeList.add("Delhi");

        spinnerFromLoc.setItems(salaryTypeList);

        spinnerFromLoc.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                String selectedType = item.toString();
                if (!selectedType.equalsIgnoreCase("Source location")) {
                    System.out.println("===========itemSelected" + selectedType);
                    selectedFromLocation = item.toString();

                } else {
                    selectedFromLocation = "";
                }
            }
        });
    }

    private void populateToLocationInSpinner() {

        List<String> salaryTypeList = new ArrayList<>();
        salaryTypeList.add(0, "Destination location");
        salaryTypeList.add("Bangalore");
        salaryTypeList.add("Chennai");
        salaryTypeList.add("Coimbatore");
        salaryTypeList.add("Delhi");

        spinnerToLoc.setItems(salaryTypeList);

        spinnerToLoc.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                String selectedType = item.toString();
                if (!selectedType.equalsIgnoreCase("Destination location")) {
                    System.out.println("===========itemSelected" + selectedType);
                    selectedToLocation = item.toString();

                } else {
                    selectedToLocation = "";
                }
            }
        });
    }

    public void hideSoftKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

}
