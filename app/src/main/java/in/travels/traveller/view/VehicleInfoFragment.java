package in.travels.traveller.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.List;

import in.travels.traveller.R;

/**
 * Created by udhaya on 5/10/18.
 */

public class VehicleInfoFragment extends Fragment {

    private MaterialSpinner spinnerTaxType;
    String selectedTaxType = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_vehicle_info,
                container, false);
        getViewCasting(view);
        return view;
    }

    private void getViewCasting(View view) {

        spinnerTaxType = (MaterialSpinner) view.findViewById(R.id.spinnerTaxType);
        populateTaxTypeInSpinner();
    }

    private void populateTaxTypeInSpinner() {

        List<String> salaryTypeList = new ArrayList<>();
        salaryTypeList.add(0, "Tax Type");
        salaryTypeList.add("Tax 1");
        salaryTypeList.add("Tax 2");
        salaryTypeList.add("Tax 3");
        salaryTypeList.add("Tax 4");

        spinnerTaxType.setItems(salaryTypeList);

        spinnerTaxType.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                String selectedType = item.toString();
                if (!selectedType.equalsIgnoreCase("Tax Type")) {
                    System.out.println("===========itemSelected" + selectedType);
                    selectedTaxType = item.toString();

                } else {
                    selectedTaxType = "";
                }
            }
        });
    }
}
