package in.travels.traveller.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;

import in.travels.traveller.R;

public class RegisterActivity extends AppCompatActivity {

    private EditText edtTxtDriverName, edtTxtLicenseNo, edtTxtDateOfIssued, edtTxtDateOfBirth,
            edtTxtBloodGroup, edtTxtMobileNum, edtTxtDateOfJoining, edtTxtAddress;
    private Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getViewCasting();
    }

    private void getViewCasting() {

        edtTxtDriverName = (EditText) findViewById(R.id.edtTxtDriverName);
        edtTxtLicenseNo = (EditText) findViewById(R.id.edtTxtLicenseNo);
        edtTxtDateOfIssued = (EditText) findViewById(R.id.edtTxtDateOfIssued);
        edtTxtDateOfBirth = (EditText) findViewById(R.id.edtTxtDateOfBirth);
        edtTxtBloodGroup = (EditText) findViewById(R.id.edtTxtBloodGroup);
        edtTxtMobileNum = (EditText) findViewById(R.id.edtTxtMobileNum);
        edtTxtDateOfJoining = (EditText) findViewById(R.id.edtTxtDateOfJoining);
        edtTxtAddress = (EditText) findViewById(R.id.edtTxtAddress);

        btnRegister = (Button) findViewById(R.id.btnRegister);

    }

}
