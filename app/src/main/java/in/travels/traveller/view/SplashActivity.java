package in.travels.traveller.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import in.travels.traveller.R;
import in.travels.traveller.db.PreferenceConnector;

/**
 * Created by udhaya on 25/9/18.
 */

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 2000;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        loadSplashScreen();
    }

    private void loadSplashScreen() {

        try {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    boolean isLoggedIn = PreferenceConnector.readBoolean(getApplicationContext(), PreferenceConnector.IS_USER_LOGGED_IN, false);


                    if (isLoggedIn) {
                        Intent dashboardIntent = new Intent(SplashActivity.this, DashboardActivity.class);
                        startActivity(dashboardIntent);
                        finish();
                    } else {
                        Intent loginIntent = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(loginIntent);
                        finish();
                    }


                }
            }, SPLASH_TIME_OUT);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
