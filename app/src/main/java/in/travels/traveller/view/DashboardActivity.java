package in.travels.traveller.view;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;

import in.travels.traveller.R;
import in.travels.traveller.db.PreferenceConnector;

public class DashboardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private FrameLayout flContent;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        PreferenceConnector.writeBoolean(DashboardActivity.this, PreferenceConnector.IS_USER_LOGGED_IN, true);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        getViewCasting();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Class fragmentClass = null;

        if (id == R.id.nav_home) {

        } else if (id == R.id.nav_driverInfo) {

            fragmentClass = DriverInfoFragment.class;
            addFragment(fragmentClass);

        } else if (id == R.id.nav_vehicleInfo) {

            fragmentClass = VehicleInfoFragment.class;
            addFragment(fragmentClass);

        } else if (id == R.id.nav_tripDetails) {

            fragmentClass = TripDetailsFragment.class;
            addFragment(fragmentClass);

        } else if (id == R.id.nav_estimate) {

        } else if (id == R.id.nav_maintenance) {

        } else if (id == R.id.nav_management) {

        } else if (id == R.id.nav_serviceInfo) {

        } else if (id == R.id.nav_help) {

        } else if (id == R.id.nav_update) {

        } else if (id == R.id.nav_logout) {
            PreferenceConnector.writeBoolean(DashboardActivity.this, PreferenceConnector.IS_USER_LOGGED_IN, false);
        }
        // Highlight the selected item has been done by NavigationView
        item.setChecked(true);
        // Set action bar title
        setTitle(item.getTitle());


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void getViewCasting() {
        flContent = (FrameLayout) findViewById(R.id.flContent);
    }

    private void addFragment(Class fragmentClass) {
        Fragment fragment = null;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

    }
}
